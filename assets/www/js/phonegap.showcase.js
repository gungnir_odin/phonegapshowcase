function getCameraBase64(){
    navigator.camera.getPicture(
        function(imageData){
            // cameraSuccess
            $('#camera_pic')
                .css('display', 'block')
                .attr('src', 'data:image/jpeg;base64,' + imageData);
        }, 
        function(message){
            // cameraError
            alert(message);
        }, 
        {
            quality : 75,
            destinationType : Camera.DestinationType.DATA_URL,
            sourceType : Camera.PictureSourceType.CAMERA,
        });
};
function getCameraURI(){
    navigator.camera.getPicture(
        function(imageURI){
            // cameraSuccess
            $('#camera_pic')
                .css('display', 'block')
                .attr('src', imageURI);
        }, 
        function(message){
            // cameraError
            alert(message);
        }, 
        {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.CAMERA,
        });
};
function getPhotoURI(){
    navigator.camera.getPicture(
        function(imageURI){
            // cameraSuccess
            $('#camera_pic')
                .css('display', 'block')
                .attr('src', imageURI);
        }, 
        function(message){
            // cameraError
            alert(message);
        }, 
        {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
        });
};
/*
 * コンパスセンサーから現在の方位を取得
 */
function watchStartCompass(){
    navigator.compass.watchHeading(
        function(heading){
            // compassSuccess
            $('#compass_heading').html('方角:'+heading.magneticHeading);
            $('#compass_timestamp').html('日付:'+new Date(heading.timestamp).toString());
            
            if( (0<=heading.magneticHeading&&heading.magneticHeading<=10)
            		|| (350<=heading.magneticHeading&&heading.magneticHeading<=360)){
                $('#kita').html('ｷﾀ━━━━(ﾟ∀ﾟ)━━━━!!');
            }else{
            	$('#kita').html('(*´ω｀*)');
            }
        },
        function(compassError){
            // compassError
            if(CompassError.COMPASS_INTERNAL_ERR==compassError.code){
            	alert('コンパスで内部エラーが発生しました');
            }else if(CompassError.COMPASS_NOT_SUPPORTED){
                alert('コンパスがサポートされていません');
            }
        },
        {
            frequency : 100,
        }
    );
}